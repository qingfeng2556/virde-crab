package cn.virde.crab.url;

import cn.virde.crab.config.CrabConfig;
import cn.virde.crab.funcinter.JedisFunc;
import cn.virde.nymph.db.redis.JedisService;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @Author Suna
 */
public class WebUrlMemory {

    private CrabConfig config;
    private JedisService jedisService;

    // Redis 键
    private String key_parse_url ;
    private String key_unparse_url ;

    public WebUrlMemory(CrabConfig config){
        this.config = config;
        this.jedisService = new JedisService(config.getRedisHost(),config.getRedisPort(),config.getRedisPassword());
        this.key_parse_url = String.format("%s:url:parse:",config.getName())+"%d";
        this.key_unparse_url = String.format("%s:url:unparse",config.getName());
    }

    /**
     * 优先弹出未解析url，
     * 当没有未解析url，弹出已解析url并解析。
     * @return
     */
    public String getParseUrl(){
        String url = jedisService.spop(key_unparse_url);
        if(url != null){
            String keyParseUrl = String.format(key_parse_url,getUrlGrade(url));
            jedisService.sadd(keyParseUrl,url);
            return url;
        }
        else{
            String keyParseUrl = String.format(key_parse_url,gradeRandom());
            return jedisService.spop(keyParseUrl);
        }
    }

    public void saddUnParseUrl(Set<String> urls){
        for(String url : urls){
            String keyParseUrl = String.format(key_parse_url,getUrlGrade(url));
            if(!jedisService.sismember(keyParseUrl,url)){
                jedisService.sadd(key_unparse_url,url);
            }
        }
    }
    public void redisDoSomeThing(JedisFunc jedisFunc){
        jedisFunc.func(jedisService);
    }

    private final static List<Integer> arrays = new ArrayList<>();
    static{
        for(int i = 0 ; i <= 5;i++){
            for(int j = 1;j<=i;j++){
                arrays.add(i);
            }
        }
    }
    public static int gradeRandom(){
        return arrays.get((int)Math.floor((Math.random()*(15))));
    }

    public int getUrlGrade(String url){

        // 判断是否在范围内，如果不在，不再扫描。
        boolean inRangeHost = false;
        for(String rangeHost : config.getRangeHosts()){
            if(url.contains(rangeHost)){
                inRangeHost = true;
                break;
            }
        }
        if(!inRangeHost) return 0;
        // 判断是否是首页，如果是，之后优先扫描。
        for(String host : config.getHosts()) {
            if(urlEqualsHost(url,host)){
                return 5;
            }
        }

        if(config.getUrlGradeHandler()!=null){
            return config.getUrlGradeHandler().getGrade(url);
        }else{
            return 5;
        }
    }

    private boolean urlEqualsHost(String url,String host){
        if(url==null||host==null)
            return false;
        if(host.endsWith("/")){
            host = host.substring(0,host.length()-1);
        }
        if(url.endsWith("/")){
            url = url.substring(0,url.length()-1);
        }
        return url.equals(host);
    }
}
