package cn.virde.crab;

import cn.virde.crab.config.CrabConfig;
import cn.virde.crab.config.PageParseConfig;
import cn.virde.crab.funcinter.PageParseFuncInter;
import cn.virde.crab.funcinter.JedisFunc;
import cn.virde.crab.funcinter.PageHrefFilterFuncInter;
import cn.virde.crab.funcinter.UrlGradeFuncInter;
import cn.virde.crab.page.PageParse;
import cn.virde.crab.url.WebUrlMemory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @Author Suna
 */
public class Crab implements Runnable{

    private Logger logger = LogManager.getLogger(Crab.class);

    private static  BlockingQueue<Runnable> queue ;
    private static  ThreadPoolExecutor executor ;

    private static final int queueSize = 10;
    private static final int corePoolSize = 5 ;
    private static final int maximumPoolSize = 10;
    private static final int keepAliveTime = 1;

    static{
        queue = new ArrayBlockingQueue<>(queueSize);
         executor = new ThreadPoolExecutor(corePoolSize,maximumPoolSize,keepAliveTime, TimeUnit.DAYS,queue);
    }

    public CrabConfig config ;
    public WebUrlMemory memory ;
    private PageParseConfig pageParseConfig;

    public Crab(CrabConfig config){
        this.config = config;
        this.pageParseConfig = new PageParseConfig();
        this.memory = new WebUrlMemory(config);
    }

    public void run() {
        memory.saddUnParseUrl(config.getHosts());
        while(true){
            if(queue.size() < queueSize){
                nextParse();
            }
            try { Thread.sleep(config.getSleepTime());
            } catch (InterruptedException e) { }

        }
    }

    private void nextParse(){
        try{
            tryNextParse();
        }catch (Exception e){
            logger.error(e);
        }
    }
    private void tryNextParse(){
        String url = memory.getParseUrl();
        if(url != null && inRangeHosts(url)){
            logger.info("Parse Url : {}",url);
            executor.execute(new PageParse(url,pageParseConfig,memory));
        }
    }

    private boolean inRangeHosts(String url){
        for(String rangeHost : config.getRangeHosts()){
            if(url.contains(rangeHost)) return true;
        }
        return false;
    }

    public Crab selfParse(PageParseFuncInter parse){
        this.pageParseConfig.setSelfParse(parse);
        return this;
    }
    public Crab hrefFilter(PageHrefFilterFuncInter filter){
        this.pageParseConfig.setHrefFilter(filter);
        return this;
    }
    public Crab proxy(String host,int port){
        this.pageParseConfig.setProxy(host,port);
        return this;
    }
    public Crab urlGradeHandler(UrlGradeFuncInter handler){
        this.config.setUrlGradeHandler(handler);
        return this;
    }
    public void redis(JedisFunc jedisFunc){
        memory.redisDoSomeThing(jedisFunc);
    }
}
