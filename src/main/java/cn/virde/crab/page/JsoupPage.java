package cn.virde.crab.page;

import cn.virde.crab.config.PageParseConfig;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class JsoupPage extends Page {

	private Document doc ;
	private PageParseConfig config;

	public JsoupPage(String url) throws IOException {
		super(url);
		this.config = new PageParseConfig();
		this.doc = Jsoup.connect(this.pageUrl).get();
	}

	public JsoupPage(String url,PageParseConfig config) throws IOException {
		super(url);
		this.config = config;
		if(config.getProxy() != null){
			this.doc = Jsoup.connect(url).proxy(config.getProxy().getHost(),config.getProxy().getPort()).get();
		}else{
			this.doc = Jsoup.connect(this.pageUrl).get();
		}
	}
	@Override
	public Set<String> getUrls() {
		return null;
	}
	/**
	 * 这个方法可能会抛空指针异常
	 */
	@Override
	public Set<String> getHrefs(){
		Set<String> hrefs = new HashSet<String>();
		Elements eles = doc.select("a[href]");
		for(Element ele : eles) {
			String href = ele.attr("abs:href");
			if(hrefFilter(href)){
				hrefs.add(href);
			}
		}
		return hrefs;
	}

	@Override
	public Set<String> getSrcs() {
		return null;
	}

	@Override
	public String getHtml() {
		return doc.html();
	}

	public Document getDoc() {
		return doc;
	}

	public void setDoc(Document doc) {
		this.doc = doc;
	}
	private boolean hrefFilter(String href){
		if(!StringUtils.isNotBlank(href)){
			return false;
		}
		if(href.endsWith(".jpg")){
			return false;
		}
		return config.hrefFilter(href);
	}
}
