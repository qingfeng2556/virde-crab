package cn.virde.crab.page;

import cn.virde.crab.config.PageParseConfig;
import cn.virde.crab.url.WebUrlMemory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Set;

/**
 * @Author Suna
 */
public class PageParse implements Runnable {

    private String url ;

    private WebUrlMemory memory;

    private PageParseConfig config;

    public PageParse(String url, PageParseConfig config,WebUrlMemory memory){
        this.url = url;
        this.memory = memory;
        this.config = config;
    }

    public void run() {
        try {
            parse();
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("页面解析出错");
        }
    }

    private void parse() throws IOException {
        JsoupPage page = new JsoupPage(url,config);
        Set<String> hrefs = page.getHrefs();
        memory.saddUnParseUrl(hrefs);
        config.doSelfParse(page);
    }

}
