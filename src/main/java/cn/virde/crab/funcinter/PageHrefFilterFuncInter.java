package cn.virde.crab.funcinter;

/**
 * @Author Suna
 */
public interface PageHrefFilterFuncInter {
    boolean filter(String href);
}
