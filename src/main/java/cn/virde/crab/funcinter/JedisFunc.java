package cn.virde.crab.funcinter;

import cn.virde.nymph.db.redis.JedisService;

/**
 * @Author Suna
 */
public interface JedisFunc {
    void func(JedisService jedis);
}
