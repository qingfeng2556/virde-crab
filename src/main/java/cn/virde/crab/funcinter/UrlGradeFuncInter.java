package cn.virde.crab.funcinter;

/**
 * @Author Suna
 */
public interface UrlGradeFuncInter {
    int getGrade(String url);
}
