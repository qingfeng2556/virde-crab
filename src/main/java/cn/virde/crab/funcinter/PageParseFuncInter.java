package cn.virde.crab.funcinter;

import cn.virde.crab.page.JsoupPage;

/**
 * @Author Suna
 */
public interface PageParseFuncInter {
    void doSelfParse(JsoupPage page);
}
