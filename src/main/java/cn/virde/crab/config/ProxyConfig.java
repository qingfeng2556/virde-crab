package cn.virde.crab.config;

/**
 * @Author Suna
 */
public class ProxyConfig {
    private String host;
    private int port;

    public ProxyConfig(String host, int port) {
        this.host = host;
        this.port = port;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
}
