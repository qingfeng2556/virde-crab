package cn.virde.crab.config;

import cn.virde.crab.funcinter.UrlGradeFuncInter;

import java.util.HashSet;

/**
 * @Author Suna
 */
public class CrabConfig {

    // 爬虫名字
    private String name ;

    // 需要爬取的网站主域名和爬虫爬取的范围
    private HashSet<String> hosts ;
    private HashSet<String> rangeHosts ;

    // redis链接信息
    private String redisHost;
    private int redisPort = 6379;
    private String redisPassword;

    private int sleepTime = 1000;

    private UrlGradeFuncInter urlGradeHandler;

    public int getSleepTime() {
        return sleepTime;
    }

    public void setSleepTime(int sleepTime) {
        this.sleepTime = sleepTime;
    }
    
    public String getRedisHost() {
        return redisHost;
    }
    public void setRedisHost(String redisHost) {
        this.redisHost = redisHost;
    }
    public int getRedisPort() {
        return redisPort;
    }
    public void setRedisPort(int redisPort) {
        this.redisPort = redisPort;
    }

    public String getRedisPassword() {
        return redisPassword;
    }

    public void setRedisPassword(String redisPassword) {
        this.redisPassword = redisPassword;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setHosts(String... hostArray) {
        if(hosts == null) hosts = new HashSet<>();
        for(String h : hostArray){
            this.hosts.add(h);
        }
    }
    public void addHost(String host){
        if(hosts == null) hosts = new HashSet<>();
        hosts.add(host);
    }

    public HashSet<String> getHosts(){
        return hosts;
    }

    public HashSet<String> getRangeHosts() {
        if(rangeHosts == null){
            rangeHosts = new HashSet<>();
            rangeHosts.addAll(hosts);
        }
        return rangeHosts;
    }

    public void addRangeHosts(String rangeHost) {
        getRangeHosts().add(rangeHost);
    }

    public UrlGradeFuncInter getUrlGradeHandler() {
        return urlGradeHandler;
    }

    public void setUrlGradeHandler(UrlGradeFuncInter urlGradeHandler) {
        this.urlGradeHandler = urlGradeHandler;
    }
}
