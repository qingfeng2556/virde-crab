package cn.virde.crab.config;

import cn.virde.crab.funcinter.PageParseFuncInter;
import cn.virde.crab.page.JsoupPage;
import cn.virde.crab.funcinter.PageHrefFilterFuncInter;

/**
 * @Author Suna
 */
public class PageParseConfig {

    private PageParseFuncInter selfParse;
    private PageHrefFilterFuncInter hrefFilter ;
    private ProxyConfig proxy;

    public void setSelfParse(PageParseFuncInter selfParse) {
        this.selfParse = selfParse;
    }

    public void setHrefFilter(PageHrefFilterFuncInter hrefFilter) {
        this.hrefFilter = hrefFilter;
    }

    public ProxyConfig getProxy() {
        return proxy;
    }

    public void setProxy(String host,int port) {
        this.proxy = new ProxyConfig(host,port);
    }
    public void doSelfParse(JsoupPage jsoupPage){
        if(selfParse!=null) selfParse.doSelfParse(jsoupPage);
    }
    public boolean hrefFilter(String href){
        if(hrefFilter != null) return hrefFilter.filter(href);
        else return true;
    }
}
